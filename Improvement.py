import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def improvement():

    "Calculate the approximation by five-point stensil analytical method"

     # Resolution and size of the domain
    N = 10                  #number of intervals
    ymin = phyProps["ymin"]
    ymax = phyProps["ymax"]
    dy = (ymax-ymin)/N

    # The spatial dimension, y:
    y = np.linspace(ymin,ymax,N+1)

    #The pressure at the y points 
    p = pressure(y, phyProps)
    uExact = uGeoExact(y, phyProps)

    # The pressure gradient and wind using five-point stensil and the exact geostropic wind
    dpdy = fivepoint_stensil(p,dy)
    u_fivepoint = geoWind(dpdy, phyProps)

    # Graph to compare the numerical and analytic solutions
    font = {'size': 14}
    plt.rc("font", **font)

    #Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k-', label ='Exact')
    plt.plot(y/1000, u_fivepoint, '*k--', label = 'Five-point stencil', ms=12, markeredgewidth = 1.5, markerfacecolor = 'none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.show()

    #Plot the error graph and compare the 2nd and five-point differences
    u = geoWind(gradient_2point(p, dy), phyProps)
    plt.plot(y/1000, u - uGeoExact(y,phyProps), 'ok--', label = '2nd order differences', ms = 12, markeredgewidth = 1.5, markerfacecolor = 'none')
    plt.plot(y/1000, u_fivepoint - uExact, '*k--', label = 'Five-point stencil differences', ms = 12, markeredgewidth = 1.5, markerfacecolor = 'none')
    plt.legend(loc = 'best')
    plt.axhline(linestyle = '-', color='k')
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    improvement() 