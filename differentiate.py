import numpy as np
# functions for calculating gradients

def gradient_2point(f,dx):
    "The gradient of one dimensional array f assuming points are a distance dx apart using 2-point differences. Returns an array the same size as f"

    # Initialised the array for the gradient to be the same size as f

    dfdx = np.zeros_like(f)

    # Two point differences at the end points
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[-1] = (f[-1]-f[-2])/dx

    # Centred differences for the mid-points
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1]-f[i-1])/(2*dx)
    return dfdx

def gradient_2ndorder(f,dx):
    
    dfdx = np.zeros_like(f)

    # calculate end-point in 2nd order

    dfdx[0] = (-3*f[0]+4*f[1]-f[2])/(2*dx)
    dfdx[-1] = (f[-3]-4*f[-2]+3*f[-1])/(2*dx)

    # Centred difference for the mid-points
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1]-f[i-1])/(2*dx)
    return dfdx

def fivepoint_stensil(f,dx):

     dfdx = np.zeros_like(f)

    # Calculate end-point in 2nd order

     dfdx[0] = (-3*f[0]+4*f[1]-f[2])/(2*dx)
     dfdx[1] = (-3*f[1]+4*f[2]-f[3])/(2*dx)
     dfdx[-2] = (f[-4]-4*f[-3]+3*f[-2])/(2*dx)
     dfdx[-1] = (f[-3]-4*f[-2]+3*f[-1])/(2*dx)

    #Centred difference for the mid-points
     for i in range(2,len(dfdx)-2):
         dfdx[i] = (f[i-2]-8*f[i-1]+8*f[i+1]-f[i+2])/(12*dx)
     return dfdx






