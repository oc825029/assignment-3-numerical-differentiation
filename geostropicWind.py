import numpy as np
import matplotlib.pyplot as plt
from numpy.core.fromnumeric import shape
from differentiate import *
from physProps import *

def geostrophicWind():
    "Calculate the geostropic with analytically and numerically and plot"

    # Resolution and size of the domain
    N = 10                  #number of intervals
    ymin = phyProps["ymin"]
    ymax = phyProps["ymax"]
    dy = (ymax-ymin)/N

    # The spatial dimension, y:
    y = np.linspace(ymin,ymax,N+1)

    #The pressure at the y points and the exact geostropic wind
    p = pressure(y, phyProps)
    uExact = uGeoExact(y, phyProps)

    # The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p,dy)
    u_2point = geoWind(dpdy, phyProps)

    # Graph to compare the numerical and analytic solutions
    font = {'size': 14}
    plt.rc("font", **font)

    #Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k-', label ='Exact')
    plt.plot(y/1000, u_2point, '*k--', label = 'Two-point differences', ms=12, markeredgewidth = 1.5, markerfacecolor = 'none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.show()

    #Plot the error graph
    plt.plot(y/1000, u_2point - uExact, '*k--', label = 'Two-point differences', ms = 12, markeredgewidth = 1.5, markerfacecolor = 'none')
    plt.legend(loc='best')
    plt.axhline(linestyle = '-', color='k')
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    geostrophicWind()







