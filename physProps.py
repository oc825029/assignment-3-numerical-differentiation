import numpy as np

phyProps = {'pa': 1e5,
            'pb': 200.,
            'f' : 1e-4,
            'rho':1.,
            'L':2.4e6,
            'ymin':0.,
            'ymax':1e6,}

def pressure(y, props):
    "The pressure and given y locations based on dictionary of physical properties, props"
    pa = props["pa"]
    pb = props["pb"]
    L = props["L"]
    return pa + pb*np.cos(y*np.pi/L)

def uGeoExact (y, props):
    "The analytic geostrophic wind at given locations, y based on dictionary of physical properties, props"
    pb = props["pb"]
    L = props["L"]
    rho = props["rho"]
    f = props["f"]
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

def geoWind(dpdy, props):
    "The geostropic wind as a function of pressure gradient based on dictionary of physical properties, props"
    rho = props["rho"]
    f = props["f"]
    return -dpdy/(rho*f)


