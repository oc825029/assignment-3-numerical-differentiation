import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *


def test():
    "test with 2nd order at two end-points"

    # Resolution and size of the domain
    N = 10                  #number of intervals
    ymin = phyProps["ymin"]
    ymax = phyProps["ymax"]
    dy = (ymax-ymin)/N

    # The spatial dimension, y:
    y = np.linspace(ymin,ymax,N+1)

    #The pressure at the y points and the exact geostropic wind
    p = pressure(y, phyProps)
    uExact = uGeoExact(y, phyProps)

    # The pressure gradient and wind using three point differences
    dpdy = gradient_2ndorder(p,dy)
    u_3point = geoWind(dpdy, phyProps)

    # Graph to compare the numerical and analytic solutions
    font = {'size': 14}
    plt.rc("font", **font)

    #Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k-', label ='Exact')
    plt.plot(y/1000, u_3point, '*k--', label = '2nd order differences', ms=12, markeredgewidth = 1.5, markerfacecolor = 'none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.show()

    #Plot the error graph
    plt.plot(y/1000, u_3point - uExact, '*k--', label = '2nd order differences', ms = 12, markeredgewidth = 1.5, markerfacecolor = 'none')
    plt.legend(loc='best')
    plt.axhline(linestyle = '-', color='k')
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    test() 


def largerN():
    "using a larger N to test the accuracy"

    # Repeat above codes but with N=100/1000
    Na = np.array([30,60])

    ymin = phyProps["ymin"]
    ymax = phyProps["ymax"]
    dy = (ymax-ymin)/Na

    lenarray = np.zeros(len(Na))
    for i in range(len(lenarray)):
        y = np.linspace(ymin, ymax, Na[i]+1)
        p = pressure(y, phyProps)
        dpdy = gradient_2point(p,dy[i])
        uExact = uGeoExact(y, phyProps)
        higherN_2point = geoWind(dpdy, phyProps)

        font = {'size': 14}
        plt.rc("font", **font)

        #Plot the approximation
        plt.plot(y/1000, uExact, 'k-', label ='Exact')
        plt.plot(y/1000, higherN_2point, 'k.-', label = 'N=%d' %Na[i], ms=12, markeredgewidth = 1.5, markerfacecolor = 'none')
        plt.legend(loc='best')
        plt.xlabel('y (km)')
        plt.ylabel('u (m/s)')
        plt.tight_layout()
        plt.show()

        #Plot the errors
        plt.plot(y/1000, higherN_2point - uExact, 'k.-', label = 'N=%d difference' %Na[i], ms = 12, markeredgewidth = 1.5, markerfacecolor = 'none')
        plt.legend(loc='best')
        plt.axhline(linestyle = '-', color='k')
        plt.xlabel('y (km)')
        plt.ylabel('u error (m/s)')
        plt.tight_layout()
        plt.show()
    
if __name__ == "__main__":
     largerN()



